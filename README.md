# README
Author: Jan Pache  
E-Mail: [jp@rsnweb.ch][2]  
  
&copy; 2017

### TODO
* RaspberryPi mit Linux booten und als lokalen Server für eine Datenbank nutzen
* NoSQL Datenbank mit [Cassandra][1] oder [MongoDB][3] aufsetzen

[1]: http://cassandra.apache.org/
[2]: mailto:jp@rsnweb.ch
[3]: https://www.mongodb.com/de