<?php
	
	include 'inc/bootstrap.inc.php';
	include CLASS_ROOT . 'Contact.php';
	include CLASS_ROOT . 'Result.php';

	$action = isset( $_REQUEST['action'] ) ? $_REQUEST['action'] : 'default';
	
	switch ( $action ) {
		
		case 'default':
			include TPL_ROOT . 'index.php';
			break;
			
		case 'submitForm':
			new \Classes\Contact( $_REQUEST );
			break;
			
		case 'result':
			new \Classes\Result();
			break;
		
		default:
			include TPL_ROOT . '404.php';
	}