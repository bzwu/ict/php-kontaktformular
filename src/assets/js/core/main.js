var bzwu = (function () {

	var self = {

		init: function () {

			console.info( 'jQuery was successfully loaded' );

			/**
			 * Init Submit Listener
			 */
			self.initSubmitListener();

			/**
			 * Init Collapse Listener
			 */
			self.initCollapseListener();
		},

		initSubmitListener: function () {

			var $submit = $( 'a.submit' );
			var $window = $( window );

			$submit.click( function ( e ) {
				self.submitContactForm( e );
			} );
			$window.keypress( function ( e ) {
				if ( e.which == 13 ) self.submitContactForm( e );
			} );

		},

		submitContactForm: function ( e ) {

			e.preventDefault(); // prevent submit for ajax-request
			$( 'p.error' ).detach();

			// Fields
			var $action = 'submitForm';
			var $gender = $( 'input[name=gender]:checked' ).val();
			var $firstname = $( 'input[name=firstname]' ).val();
			var $lastname = $( 'input[name=lastname]' ).val();
			var $address = $( 'input[name=address]' ).val();
			var $postcode = $( 'input[name=postcode]' ).val();
			var $location = $( 'input[name=location]' ).val();
			var $country = $( 'select[name=country]' ).find( ':selected' ).val();
			var $paymentmethod = $( 'input[name=paymentmethod]:checked' ).val();
			var $calculations = [];
			var $message = $( 'textarea[name=message]' ).val();
			try {
				var $captcha = grecaptcha.getResponse();
			} catch ( e ) {
				$captcha = null;
			}

			$( 'input[name=calculations]:checked' ).each( function () {
				$calculations.push( $( this ).val() );
			} );

			var $data = {
				action: $action,
				gender: $gender,
				firstname: $firstname,
				lastname: $lastname,
				address: $address,
				postcode: $postcode,
				location: $location,
				country: $country,
				paymentmethod: $paymentmethod,
				calculations: $calculations,
				message: $message,
				captcha: $captcha
			};

			$.ajax( {
				method: 'POST',
				url: '/',
				data: $data,
				success: function ( data, textStatus, jqXHR ) {
					var $data = JSON.parse( data );
					swal( {
						type: 'success',
						title: 'Danke!',
						text: $data.text
					}, function () {
						window.location.reload();
					} );
				},
				error: function ( jqXHR, textStatus, errorThrown ) {
					var $data = JSON.parse( jqXHR.responseText );
					swal( {
						type: 'error',
						title: 'Whoops!',
						text: $data.text
					} );
					var $valFields = $.makeArray( $data.valFields );
					var $errorFields = $.map( $valFields, function( k, v ) { return [k, v] } );
					$.each( $errorFields[0], function ( k, v ) {
						$( '<p class="error">' + v + '</p>' ).appendTo( 'div[input-name=' + k + ']' );
					} );

					if ( $data.error == 2 ) {
						grecaptcha.reset();
					}
				}
			} );
		},

		initCaptcha: function () {

			var $captchaId = 'captcha';
			var $captchaObj = $( '#' + $captchaId );

			grecaptcha.render( $captchaId, {
				'sitekey': '6Ld3URkUAAAAADcoMxeD9oHi5J7RjKYG5C-KHpfT',
				'theme': 'dark'
			} );
		},

		initCollapseListener: function () {

			var $collBtn = $( '.collapse-button' );

			$collBtn.click( function ( e ) {
				var $target = $( '[data-to-collapse=' + $( this ).attr( 'data-for-collapse' ) + ']' );
				var $icon = $( this ).find( '.collapse-icon' );
				$( this ).toggleClass( 'open' );
				if ( $( this ).hasClass( 'open' ) ) {
					$target.fadeIn( 'slow' );
				} else {
					$target.fadeOut( 'slow' );
				}
			} );
		}
	};

	return {
		init: self.init,
		initCaptcha: self.initCaptcha
	}

} )();

jQuery( document ).ready( function () {

	bzwu.init();
} );