CREATE TABLE IF NOT EXISTS `ict_contact` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gender` varchar(1) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `paymentmethod` varchar(100) DEFAULT NULL,
  `message` text,
  `captcha` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;