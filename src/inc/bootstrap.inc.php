<?php

/* Config */
ini_set( 'display_errors', '1' );
date_default_timezone_set( 'Europe/Berlin' );

/* Constants */
define( 'PROJ_ROOT', dirname( dirname( __FILE__ ) ) . '/' );
define( 'TPL_ROOT', PROJ_ROOT . 'templates/' );
define( 'CLASS_ROOT', PROJ_ROOT . 'classes/' );
define( 'REQ_ROOT', PROJ_ROOT . 'requests/' );
define( 'CSV_DELIM', ';' );
define( 'WRITE_CSV', 0 );
define( 'WRITE_DB', 1 );
define( 'REQ_FILE_NAME', gmdate( 'Y_m', time() ) );
define( 'SECRET_KEY', '6Ld3URkUAAAAANEp3clVVJvFMlkehElnw78CoyD_' );
define( 'DB_HOST', '192.168.99.100' ); // 127.0.0.1 | localhost
define( 'DB_DATABASE', 'ict' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', 'fdglkjfjklhdfg67' );
define( 'SQL_INIT_FILE', PROJ_ROOT . 'inc/install.sql' );

/* global includes */
include_once PROJ_ROOT . 'lib/autoload.php';