<!doctype html>
<html>
<head>
	
	<title>Kontaktformular</title>
	
	<!-- CSS -->
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
	<link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">

	<!-- Icons -->
	<link rel="shortcut icon" type="image/x-icon" href="assets/img/icons/favicon.ico">
	<link rel="icon" type="image/x-icon" href="assets/img/icons/favicon.ico">
	<link rel="icon" type="image/gif" href="assets/img/icons/favicon.gif">
	<link rel="icon" type="image/png" href="assets/img/icons/favicon.png">
	<link rel="apple-touch-icon" href="assets/img/icons/apple-touch-icon.png">
	<link rel="apple-touch-icon" href="assets/img/icons/apple-touch-icon-57x57.png" sizes="57x57">
	<link rel="apple-touch-icon" href="assets/img/icons/apple-touch-icon-60x60.png" sizes="60x60">
	<link rel="apple-touch-icon" href="assets/img/icons/apple-touch-icon-72x72.png" sizes="72x72">
	<link rel="apple-touch-icon" href="assets/img/icons/apple-touch-icon-76x76.png" sizes="76x76">
	<link rel="apple-touch-icon" href="assets/img/icons/apple-touch-icon-114x114.png" sizes="114x114">
	<link rel="apple-touch-icon" href="assets/img/icons/apple-touch-icon-120x120.png" sizes="120x120">
	<link rel="apple-touch-icon" href="assets/img/icons/apple-touch-icon-128x128.png" sizes="128x128">
	<link rel="apple-touch-icon" href="assets/img/icons/apple-touch-icon-144x144.png" sizes="144x144">
	<link rel="apple-touch-icon" href="assets/img/icons/apple-touch-icon-152x152.png" sizes="152x152">
	<link rel="apple-touch-icon" href="assets/img/icons/apple-touch-icon-180x180.png" sizes="180x180">
	<link rel="apple-touch-icon" href="assets/img/icons/apple-touch-icon-precomposed.png">
	<link rel="icon" type="image/png" href="assets/img/icons/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="assets/img/icons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="assets/img/icons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="assets/img/icons/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="assets/img/icons/favicon-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="assets/img/icons/favicon-196x196.png" sizes="196x196">
	<meta name="msapplication-TileImage" content="assets/img/icons/win8-tile-144x144.png">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-navbutton-color" content="#ffffff">
	<meta name="msapplication-square70x70logo" content="assets/img/icons/win8-tile-70x70.png">
	<meta name="msapplication-square144x144logo" content="assets/img/icons/win8-tile-144x144.png">
	<meta name="msapplication-square150x150logo" content="assets/img/icons/win8-tile-150x150.png">
	<meta name="msapplication-wide310x150logo" content="assets/img/icons/win8-tile-310x150.png">
	<meta name="msapplication-square310x310logo" content="assets/img/icons/win8-tile-310x310.png">

</head>
<body>