<?php
	$res = $_REQUEST['result'];
	include TPL_ROOT . 'header.php';
?>

	<div class="result">
		<h1>Results</h1>
		<p>Total: <b><?= $res['submissions'] ?></b> submissions</p>
		
		<!-- Genders -->
		<div id="genders" class="item">
			<h3>Genders</h3>
			<div id="gender-chart" class="chart" style="height: 250px;"></div>
		</div>
		
		<!-- Firstnames -->
		<div id="firstnames" class="item">
			<div class="collapse-button" data-for-collapse="firstnames">
				<p>Firstnames</p>
				<div class="collapse-icon">
					<span>&nbsp;</span>
					<span>&nbsp;</span>
				</div>
			</div>
			<div class="collapse-container" data-to-collapse="firstnames">
				<table>
					<tr>
						<th>Nr.</th>
						<th>Firstnames</th>
					</tr>
					<?php foreach ($res['firstname'] as $nr => $firstname) { ?>
						<tr>
							<td><?= $nr + 1 ?></td>
							<td><?= $firstname ?></td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
		
		<!-- Lastnames -->
		<div id="lastnames" class="item">
			<div class="collapse-button" data-for-collapse="lastnames">
				<p>Lastnames</p>
				<div class="collapse-icon">
					<span>&nbsp;</span>
					<span>&nbsp;</span>
				</div>
			</div>
			<div class="collapse-container" data-to-collapse="lastnames">
				<table>
					<tr>
						<th>Nr.</th>
						<th>Lastnames</th>
					</tr>
					<?php foreach ($res['lastname'] as $nr => $lastname) { ?>
						<tr>
							<td><?= $nr + 1 ?></td>
							<td><?= $lastname ?></td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
		
		<!-- Addresses -->
		<div id="addresses" class="item">
			<div class="collapse-button" data-for-collapse="addresses">
				<p>Addresses</p>
				<div class="collapse-icon">
					<span>&nbsp;</span>
					<span>&nbsp;</span>
				</div>
			</div>
			<div class="collapse-container" data-to-collapse="addresses">
				<table>
					<tr>
						<th>Nr.</th>
						<th>Addresses</th>
					</tr>
					<?php foreach ($res['address'] as $nr => $address) { ?>
						<tr>
							<td><?= $nr + 1 ?></td>
							<td><?= $address ?></td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
		
		<!-- Postcodes -->
		<div id="postcodes" class="item">
			<div class="collapse-button" data-for-collapse="postcodes">
				<p>Locations</p>
				<div class="collapse-icon">
					<span>&nbsp;</span>
					<span>&nbsp;</span>
				</div>
			</div>
			<div class="collapse-container" data-to-collapse="postcodes">
				<table>
					<tr>
						<th>Nr.</th>
						<th>Locations</th>
					</tr>
					<?php foreach ($res['postcode'] as $nr => $postcode) { ?>
						<tr>
							<td><?= $nr + 1 ?></td>
							<td><?= $postcode . ' ' . $res['location'][$nr] ?></td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
		
		<!-- Messages -->
		<div id="messages" class="item">
			<div class="collapse-button" data-for-collapse="messages">
				<p>Messages</p>
				<div class="collapse-icon">
					<span>&nbsp;</span>
					<span>&nbsp;</span>
				</div>
			</div>
			<div class="collapse-container" data-to-collapse="messages">
				<table>
					<tr>
						<th>Nr.</th>
						<th>Message</th>
					</tr>
					<?php foreach ($res['message'] as $nr => $message) { ?>
						<tr>
							<td><?= $nr + 1 ?></td>
							<td><?= $message ?></td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>

		<!-- Countries -->
		<div id="countries" class="item">
			<h3>Countries</h3>
			<div id="country-chart" class="chart" style="height: 250px;"></div>
		</div>

		<!-- Payment Method -->
		<div id="payments" class="item">
			<h3>Payment Methods</h3>
			<div id="payment-chart" class="chart" style="height: 250px;"></div>
		</div>
	</div>

<?php include TPL_ROOT . 'footer.php' ?>

<script type="text/javascript">
	var $results = {};
	var $genders = [];
	var $countries = [];
	var $pm = [];
	$results = <?= json_encode( $res ) ?>;
	
	/**
	 * Donut chart for genders
	 */
	$genders['m'] = 0;
	$genders['f'] = 0;
	$.each( $results.gender, function ( k, $gender ) {
		$genders[$gender]++;
	} );
	
	Morris.Donut( {
		element: 'gender-chart',
		data: [
			{label: 'M', value: $genders['m']},
			{label: 'F', value: $genders['f']}
		]
	} );

	/**
	 * Donut chart for countries
	 */
	$countries['ch'] = 0;
	$countries['de'] = 0;
	$countries['at'] = 0;
	$.each( $results.country, function ( k, $country ) {
		$countries[$country]++;
	} );
	Morris.Donut( {
		element: 'country-chart',
		data: [
			{label: 'Schweiz', value: $countries['ch']},
			{label: 'Deutschland', value: $countries['de']},
			{label: 'Österreich', value: $countries['at']}
		]
	} );

	/**
	 * Donut chart for payment methods
	 */
	$pm['mastercard'] = 0;
	$pm['american'] = 0;
	$pm['visa'] = 0;
	$.each( $results.paymentmethod, function ( k, $method ) {
		$pm[$method]++;
	} );
	Morris.Donut( {
		element: 'payment-chart',
		data: [
			{label: 'Mastercard', value: $pm['mastercard']},
			{label: 'American Express', value: $pm['american']},
			{label: 'Visa', value: $pm['visa']}
		]
	} );
</script>
