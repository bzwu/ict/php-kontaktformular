<?php include TPL_ROOT . 'header.php' ?>

	<div class="form">
		<h1>Kontaktformular</h1>
		<form id="contactForm">

			<div class="field" input-name="gender">
				<label>Anrede <sup>*</sup></label>
				<div class="option">
					<input type="radio" id="inputMale" name="gender" value="m">
					<label for="inputMale">Herr</label>
				</div>
				<div class="option">
					<input type="radio" id="inputFemale" name="gender" value="f">
					<label for="inputFemale">Frau</label>
				</div>
			</div>

			<div class="field" input-name="firstname">
				<label for="inputFirstname">Vorname <sup>*</sup></label>
				<input type="text" id="inputFirstname" name="firstname">
			</div>

			<div class="field" input-name="lastname">
				<label for="inputLastname">Nachname <sup>*</sup></label>
				<input type="text" id="inputLastname" name="lastname">
			</div>

			<div class="field" input-name="address">
				<label for="inputAddress">Adresse <sup>*</sup></label>
				<input type="text" id="inputAddress" name="address">
			</div>

			<div class="field" input-name="postcode">
				<label for="inputPostCode">PLZ <sup>*</sup></label>
				<input type="number" id="inputPostCode" name="postcode">
			</div>

			<div class="field" input-name="location">
				<label for="inputLocation">Wohnort <sup>*</sup></label>
				<input type="text" id="inputLocation" name="location">
			</div>

			<div class="field" input-name="country">
				<label for="inputCountry">Land <sup>*</sup></label>
				<!--<input type="text" id="inputCountry" name="country">-->
				<select id="inputCountry" name="country">
					<option value="ch" selected>Schweiz</option>
					<option value="de">Deutschland</option>
					<option value="at">Österreich</option>
				</select>
			</div>

			<div class="field" input-name="paymentmethod">
				<label>Zahlungsmethode <sup>*</sup></label>
				<div class="option">
					<input type="radio" id="inputMastercard" name="paymentmethod" value="mastercard">
					<label for="inputMastercard">Mastercard</label>
				</div>
				<div class="option">
					<input type="radio" id="inputAmerican" name="paymentmethod" value="american">
					<label for="inputAmerican">American Express</label>
				</div>
				<div class="option">
					<input type="radio" id="inputVisa" name="paymentmethod" value="visa">
					<label for="inputVisa">Visa</label>
				</div>
			</div>

			<div class="field" input-name="calculations">
				<label>Wählen Sie alle richtigen Rechnungen</label>
				<div class="option">
					<input type="checkbox" id="inputCalc1" name="calculations" value="1">
					<label for="inputCalc1">1 + 1 = 2</label>
				</div>
				<div class="option">
					<input type="checkbox" id="inputCalc2" name="calculations" value="0">
					<label for="inputCalc2">30 &divide; 10 = 2</label>
				</div>
				<div class="option">
					<input type="checkbox" id="inputCalc3" name="calculations" value="1">
					<label for="inputCalc3"> 0 &divide; 2 = undefined</label>
				</div>
			</div>

			<div class="field" input-name="message">
				<label for="inputMessage">Anliegen <sup>*</sup></label>
				<textarea id="inputMessage" name="message"></textarea>
			</div>

			<div id="captcha" input-name="captcha"></div>

			<a class="submit">Senden</a>
		</form>

		<p><sup>*</sup> = Pflichtfeld</p>
	</div>

	<script type="text/javascript">
		var captcha = function () {
			bzwu.initCaptcha();
		}
	</script>
	<script src="https://www.google.com/recaptcha/api.js?hl=de-CH&onload=captcha&render=explicit" async defer></script>
	
<?php include TPL_ROOT . 'footer.php' ?>