<?php
	
	namespace Classes;
	
	use Exception;
	use PDO;
	use PDOException;
	
	class Builder
	{
		/**
		 * @var array
		 */
		private $endDataKeys = [];
		
		/**
		 * @var array
		 */
		private $endData = [];
		
		/**
		 * @var resource
		 */
		private $stream;
		
		/**
		 * @var string
		 */
		private $filepath;
		
		/**
		 * @var \PDO
		 */
		private $db;
		
		/**
		 * Constructor
		 *
		 * @param array $values  Validated user inputs
		 * @param array $request Whole request array
		 *
		 * @return boolean $success False on error else true
		 */
		public function __construct( $values, $request ) {
			
			foreach ( $values as $k => $v ) {
				if ( array_key_exists( $k, $request ) ) {
					array_push( $this->endDataKeys, $k );
					array_push( $this->endData, $request[$k] );
				}
			}
			// implode calculations to one string
			foreach ( $this->endData as $k => $v ) {
				if ( is_array( $v ) )
					$this->endData[$k] = implode( $this->endData[$k] );
			}
			// Escape html chars for all data
			foreach ( $this->endData as $k => $data ) {
				$this->endData[$k] = $this->escapeHTML( $data );
			}
			if ( WRITE_CSV )
				$this->runCSV();
			if ( WRITE_DB )
				$this->runDB();
			
			return true;
		}
		
		/**
		 * Insert the data into a
		 * monthly csv file
		 *
		 * @return boolean $success
		 */
		private function runCSV() {
			
			foreach ( $this->endData as $k => $v ) {
				$this->endData[$k] = $this->makeFine( $v );
			}
			$this->openStream( REQ_FILE_NAME );
			fputcsv( $this->stream, $this->endData, CSV_DELIM );
			$this->closeStream( $this->stream );
			
			return file_exists( $this->filepath );
		}
		
		/**
		 * Insert the data into
		 * the Database
		 */
		private function runDB() {
			
			try {
				$stmt = Database::get()->prepare( 'INSERT INTO ict_contact (gender, firstname, lastname, address, postcode, location, country, paymentmethod, message, captcha) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)' );
				$stmt->execute( [
					in_array( 'gender', $this->endDataKeys ) ? $this->endData[array_search( 'gender', $this->endDataKeys )] : NULL,
					in_array( 'firstname', $this->endDataKeys ) ? $this->endData[array_search( 'firstname', $this->endDataKeys )] : NULL,
					in_array( 'lastname', $this->endDataKeys ) ? $this->endData[array_search( 'lastname', $this->endDataKeys )] : NULL,
					in_array( 'address', $this->endDataKeys ) ? $this->endData[array_search( 'address', $this->endDataKeys )] : NULL,
					in_array( 'postcode', $this->endDataKeys ) ? $this->endData[array_search( 'postcode', $this->endDataKeys )] : NULL,
					in_array( 'location', $this->endDataKeys ) ? $this->endData[array_search( 'location', $this->endDataKeys )] : NULL,
					in_array( 'country', $this->endDataKeys ) ? $this->endData[array_search( 'country', $this->endDataKeys )] : NULL,
					in_array( 'paymentmethod', $this->endDataKeys ) ? $this->endData[array_search( 'paymentmethod', $this->endDataKeys )] : NULL,
					in_array( 'message', $this->endDataKeys ) ? $this->endData[array_search( 'message', $this->endDataKeys )] : NULL,
					in_array( 'captcha', $this->endDataKeys ) ? $this->endData[array_search( 'captcha', $this->endDataKeys )] : NULL
				] );
			} catch ( PDOException $e ) {
				die( $e->getMessage() );
			}
		}
		
		/**
		 * Checks if output file
		 * exists. If not it's going to be
		 * created and added to GIT
		 *
		 * @param string $filename
		 */
		private function openStream( $filename )
		{
			$this->filepath = REQ_ROOT . $filename . '.csv';
			if ( !file_exists( $this->filepath ) ) {
				file_put_contents( $this->filepath, implode( CSV_DELIM, $this->endDataKeys ) . PHP_EOL );
				shell_exec( 'cd ../../' );
				shell_exec( 'git add .' );
			}
			try {
				$this->stream = fopen( $this->filepath, 'a' );
			} catch ( Exception $e ) {
				echo $e->getTraceAsString();
			}
		}
		
		/**
		 * Close a handle
		 *
		 * @param resource $stream
		 */
		private function closeStream( $stream )
		{
			fclose( $stream );
		}
		
		/**
		 * @param string $string
		 *
		 * @return string
		 */
		private function makeFine( $string )
		{
			return str_replace( ['ä', 'ü', 'ö', 'Ä', 'Ü', 'Ö', 'ß'], ['ae', 'ue', 'oe', 'Ae', 'Ue', 'Oe', 'ss'], $string );
		}
		
		/**
		 * @param string $string
		 *
		 * @return string
		 */
		private function escapeHTML( $string )
		{
			return htmlentities( $string );
		}
	}