<?php
	
	namespace Classes;

	include "Builder.php";
	
	class Contact
	{
		/**
		 * @var array
		 */
		private $request = [];
		
		/**
		 * @var array
		 */
		private $valFields = [];
		
		/**
		 * @var array
		 */
		private $arrResponse = ['error' => 0, 'valFields' => [], 'text' => ''];
		
		/**
		 * @var string
		 */
		private $googleAPI = 'https://www.google.com/recaptcha/api/siteverify';
		
		/**
		 * Constructor
		 *
		 * @param array $request
		 */
		public function __construct( $request ) {

			$this->request = $request;
			/*
			 * no-val = no validation
			 * NULL = empty/isset check
			 * int = int check
			 * calc = calculation check
			 * recaptcha = validate captcha
			 * email = check for valid Email
			 */
			$this->valFields = [
				'gender' => NULL,
				'firstname' => NULL,
				'lastname' => NULL,
				'address' => NULL,
				'postcode' => 'int',
				'location' => NULL,
				'country' => NULL,
				'paymentmethod' => NULL,
				'calculations' => 'no-val',
				'message' => NULL,
				'captcha' => 'no-val' // TODO set to 'recaptcha'
			];

			$this->validate();
			if ( $this->arrResponse['error'] == 1 ) {
				$this->error( 400, 'Bitte prüfe deine Angaben!' );
			} else if ( $this->arrResponse['error'] == 2 ) {
				$this->error( 403, 'Captcha konnte nicht validiert werden!' );
			} else if ( !new Builder( $this->valFields, $this->request ) ) {
				$this->error( 500, 'Could not build form data!' );
			} else {
				$this->success( 'Vielen Dank für\'s Mitmachen!' );
			}
		}
		
		/**
		 * Validator
		 * -------------
		 *
		 * Validates Data that is given
		 * as array combined with some
		 * information about the validation
		 */
		private function validate() {
			
			foreach ( $this->valFields as $k => $v ) {
				if ( $v != 'no-val' ) {
					$item = isset( $this->request[$k] ) ? $this->request[$k] : NULL;

					if ( !isset( $item ) || empty( $item ) ) {
						$this->arrResponse['error'] = 1;
						$this->arrResponse['valFields'][$k] = 'Dieses Feld muss ausgefüllt werden!';
					} else {
						switch ( $v ) {

							case 'int':
								if ( !is_int( intval( $item ) ) ) {
									$this->arrResponse['error'] = 1;
									$this->arrResponse['valFields'][$k] = 'Nur Zahlen sind zulässig!';
								}
								break;

							case 'calc':
								$trueCounter = 0;
								foreach ( $item as $calculation ) {
									if ( !$calculation ) {
										$this->arrResponse['error'] = 1;
										$this->arrResponse['valFields'][$k] = 'Die Auswahl ist fehlerhaft!';
									} else {
										$trueCounter++;
									}
								}

								if ( $trueCounter < 2 ) {
									$this->arrResponse['error'] = 1;
									$this->arrResponse['valFields'][$k] = 'Die Auswahl ist nicht vollständig!';
								}
								break;

							case 'recaptcha':
								$data = ['secret' => SECRET_KEY, 'response' => $item];

								$options = [
									'http' => [
										'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
										'method'  => 'POST',
										'content' => http_build_query( $data )
									]
								];
								$context  = stream_context_create( $options );
								$json = file_get_contents( $this->googleAPI, false, $context );
								if ( $json === FALSE ) {
									$this->arrResponse['error'] = 1;
									$this->arrResponse['valFields'][$k] = 'Die Captcha Abfrage konnte nicht ausgeführt werden!';
								}
								$result = json_decode( $json, true );

								if ( !$result['success'] ) {
									$this->arrResponse['error'] = 2;
									$this->arrResponse['valFields'][$k] = 'Captcha check war nicht erfolgreich!';
								}
								break;
								
							case 'email':
								if ( !filter_var( $item, FILTER_VALIDATE_EMAIL ) ) {
									$this->arrResponse['error'] = 1;
									$this->arrResponse['valFields'][$k] = 'Die eingegebene E-Mail Adresse ist nicht gültig!';
								}
								break;
						}
					}
				}
			}
		}
		
		/**
		 * @param null|string $text
		 */
		private function success( $text = NULL ) {
			
			if ( !is_null( $text ) )
				$this->arrResponse['text'] = $text;
			
			http_response_code( 200 );
			die( json_encode( $this->arrResponse ) );
		}
		
		/**
		 * @param int         $code
		 * @param null|string $text
		 */
		private function error( $code = 500, $text = NULL ) {
			
			if ( !is_null( $text ) )
				$this->arrResponse['text'] = $text;
			
			http_response_code( (int)$code );
			die( json_encode( $this->arrResponse ) );
		}
	}