<?php

namespace classes;

class Database
{
	/**
	 * @var Database
	 */
	private static $_instance = NULL;
	
	/**
	 * @var \PDO
	 */
	private $database;
	
	private function __construct()
	{
		$this->database = new \PDO( sprintf( 'mysql:host=%s;dbname=%s;charset=utf8;port=3306', DB_HOST, DB_DATABASE ), DB_USER, DB_PASSWORD );
		$this->initDB();
	}
	
	/**
	 * Checks on every request if
	 * the needed dependencies are given.
	 * If not, they're going to create it
	 */
	private function initDB()
	{
		$this->database->exec( SQL_INIT_FILE );
	}
	
	/**
	 * @return \PDO
	 */
	public static function get()
	{
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		
		return self::$_instance->database;
	}
}