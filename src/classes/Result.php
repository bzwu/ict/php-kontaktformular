<?php
	
namespace Classes;

use Exception;

class Result
{
	/**
	 * @var string $filedata Raw submitted data
	 */
	protected $filedata;
	
	/**
	 * @var array $data Processed filedata
	 */
	protected $data;
	
	/**
	 * @var array $chart Data sorted with keys and counts
	 */
	protected $chart;
	
	/**
	 * Constructor
	 * ------------
	 *
	 * Runs all needed methods
	 */
	public function __construct()
	{
		$this->readFileData();
		$this->processData();
		$this->prepareChart();
		$this->render();
	}
	
	/**
	 * Get all the file data
	 */
	private function readFileData()
	{
		$file = $this->getFileName();
		
		if ( file_exists( $file ) ) {
			try {
				$stream = fopen( $file, 'r' );
				$this->filedata = fread( $stream, filesize( $file ) );
				fclose( $stream );
			} catch ( Exception $e ) {
				echo $e->getTraceAsString();
			}
		} else {
			$this->error( 404, 'There are no requests for this month' );
		}
	}
	
	/**
	 * Prepare the data for the final output
	 */
	private function processData()
	{
		$raw = explode( PHP_EOL, $this->filedata );
		$result = [];
		foreach ( $raw as $k => $v ) {
			if ( !empty( $v ) )
				$result[$k] = explode( CSV_DELIM, $v );
		}
		$this->data = $result;
	}
	
	/**
	 * Prepare the data for
	 * the charts (escape critical stuff etc.)
	 */
	private function prepareChart()
	{
		$this->chart['submissions'] = 0;
		
		foreach ( $this->data as $k1 => $v1 ) {
			foreach ( $v1 as $k2 => $v2 ) {
				if ( $k1 == 0 ) {
					$this->chart[$v2] = [];
				} else {
					// TODO @jpache: Remove debug
					echo '<pre>';
					die( var_dump($this->data) );
					array_push( $this->chart[$this->data[0][$k2]], ltrim( rtrim( $v2, "\r\"\n\t " ), "\r\"\n\t " ) );
				}
			}
			if ( $k1 != 0 ) {
				$this->chart['submissions']++;
			}
		}
	}
	
	/**
	 * Render the charts (include file)
	 * And set the processed data into the request
	 */
	private function render()
	{
		$_REQUEST['result'] = $this->chart;
		include TPL_ROOT . 'result.php';
	}
	
	/**
	 * Get the current csv file for
	 * the given date
	 *
	 * @return string
	 */
	private function getFileName()
	{
		$filename = '';
		if ( isset( $_REQUEST['e'] ) && !(empty( $_REQUEST['e'] ) || is_null( $_REQUEST['e'] ) ) ) {
			$filename = REQ_ROOT . gmdate( 'Y', time() ) . '_' . trim( $_REQUEST['e'] ) . '.csv';
		} else {
			$filename = REQ_ROOT . REQ_FILE_NAME . '.csv';
		}
		
		return $filename;
	}
	
	/**
	 * @param null|string $text
	 */
	private function success( $text = NULL )
	{
		http_response_code( 200 );
		if ( !is_null( $text ) )
			die( $text );
		exit;
	}
	
	/**
	 * @param int  $code
	 * @param null|string $text
	 */
	private function error( $code = 500, $text = NULL )
	{
		http_response_code( $code );
		if ( !is_null( $text ) )
			die( $text );
		exit;
	}
}